<?php

namespace App\Http\Controllers\Admin;

use App\Reservation;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function index()
    {
        $reservation = Reservation::all();
        return view('admin.reservations.index',compact('reservation'));
    }

    public function status($id)
    {
        $reservation = Reservation::find($id);
        $reservation->status = true;
        $reservation->save();
        Toastr::success('Reservations successfully confirmed','success',["positionClass"=>"toast-top-right"]);
        return redirect()->back();
    }

    public function destroy($id)
    {
        Reservation::find($id)->delete();
        Toastr::success('Reservations Deleted successfully','success',["positionClass"=>"toast-top-right"]);
        return redirect()->back();
    }
}
