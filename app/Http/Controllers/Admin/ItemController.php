<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\ItemRequest;
use App\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class ItemController extends Controller
{
    const UPLOAD_DIR = '/uploads/items/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name','id');
        return view('admin.items.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $data = $request->only('name','description','image','price','category_id');

        if ($request->hasFile('image')) {

            $data['image'] = $this->upload($request->image);
        }
        Item::create($data);


        session()->flash('message','Item Created Successfully');
        return redirect('admin/items');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $categories = Category::pluck('name','id');
        return view('admin.items.edit', compact('item','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, Item $item)
    {
        $data = $request->only('name','description','image','price','category_id');

        if ($request->hasFile('image')) {
            $data['image'] = $this->upload($request->image);
        }

        $this->unlink($item->image);
        $item->update($data);

        session()->flash('message','Item Updated Successfully');
        return redirect('admin/items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $this->unlink($item->image);
        $item->delete();
        session()->flash('message','Item Deleted Successfully');
        return redirect('admin/items');
    }

    private function upload($file, $title = ''){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-'.$title . '.' .$file->getClientOriginalExtension();
        Image::make($file)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)){
            @unlink(public_path(). self::UPLOAD_DIR . $file);
        }
    }
}
