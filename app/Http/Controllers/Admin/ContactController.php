<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();
        return view('admin.contacts.index',compact('contacts'));
    }

    public function show($id)
    {
        $contacts = Contact::all();
        return view('admin.contacts.show', compact('contacts'));
    }

    public function destroy($id)
    {
       Contact::find($id)->delete();
       Toastr::success('Contact message successfully Deleted','success',["positionClass"=>"toast-top-right"]);
       return redirect()->back();
    }
}
