<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SliderRequest;
use App\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class SliderController extends Controller
{
    const UPLOAD_DIR = '/uploads/sliders/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sliders = Slider::all();
        return view('admin.sliders.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        $data = $request->only('title','sub_title','image');

        if ($request->hasFile('image')) {

            $data['image'] = $this->upload($request->image);
        }
        Slider::create($data);


        session()->flash('message','Slider Created Successfully');
        return redirect('admin/sliders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
       return view('admin.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, Slider $slider)
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $data['image'] = $this->upload($request->image);
        }
        $this->unlink($slider->image);
        $slider->update($data);
        session()->flash('message','Slider Updated Successfully');
        return redirect('admin/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $this->unlink($slider->image);
        $slider->delete();
        session()->flash('message','Slider Deleted Successfully');
        return redirect('admin/sliders');
    }

    private function upload($file, $title = ''){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-'.$title . '.' .$file->getClientOriginalExtension();
        Image::make($file)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)){
            @unlink(public_path(). self::UPLOAD_DIR . $file);
        }
    }
}
