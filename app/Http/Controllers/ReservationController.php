<?php

namespace App\Http\Controllers;

use App\Reservation;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function reserve(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'email' => 'required|email'
        ]);

//        $data = $request->only('name','phone','email','message','status','date');
//        Reservation::create($data);
        $reservation = new Reservation();
        $reservation->name = $request->name;
        $reservation->phone = $request->phone;
        $reservation->email = $request->email;
        $reservation->message = $request->message;
        $reservation->date = $request->date;
        $reservation->status = false;
        $reservation->save();

        Toastr::success('Reservation request successfully', 'success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();

    }
}
