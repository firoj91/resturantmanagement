<?php

namespace App\Http\Controllers;

use App\Contact;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function sendMessage(Request $request)
    {
        $request->validate([
            'name'=>'required|min:5',
            'email'=>'required|email',
            'subject'=>'required',
            'message'=>'required',

        ]);

        $data = $request->only('name','email','subject','message');
        Contact::create($data);
        Toastr::success('Message request successfully', 'success', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
}
