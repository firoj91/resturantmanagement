<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">


    <title>admin login</title>

    <!-- Bootstrap CSS -->
    <link href="{{asset('backend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/bootstrap-theme.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/elegant-icons-style.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/style-responsive.css')}}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <script src="{{asset('backend/js/html5shiv.js')}}"></script>

    <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->
</head>

<body class="login-img3-body">
<div class="container">
    <div class="row ">
        <form method="POST" class="login-form" action="{{ route('login') }}">
            @csrf
            <div class="login-wrap">
                <p class="login-img"><i class="material-icons">lock_outline</i></p>
                <div class="input-group">
                    <span class="input-group-addon"><i class="material-icons">email</i></span>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                             <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="material-icons">vpn_key</i></span>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                              <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>

                <div class="form-group row">
                     <div class="col-md-6 ">
                         <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>
