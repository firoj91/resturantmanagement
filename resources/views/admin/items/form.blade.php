<div class="row">
    <div class="col-md-12">
        <div class="form-group ">
            {!! Form::label('category_id','Category',[ "class"=>"control-label"]) !!}
            {!! Form::select('category_id',$categories, null,["placeholder"=>"Please Select category","class"=>"form-control",  'autofocus']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('name','Name',[ "class"=>"control-label"]) !!}
            {!! Form::text('name', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('description','Description',[ "class"=>"control-label"]) !!}
            {!! Form::textarea('description', null,["class"=>"form-control",  'autofocus']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="control-label">Image</label>
        <input type="file" name="image">
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('price','Price',[ "class"=>"control-label"]) !!}
            {!! Form::number('price', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>



