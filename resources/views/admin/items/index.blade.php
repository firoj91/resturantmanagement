@extends('admin.layouts.master')
@section('title','Item')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">All Item
                    <span class="pull-right"><a href="{{url('admin/items/create')}}" ><i class="material-icons">note_add</i>Add New</a></span>
                </h4>

            </div>
            <div class="card-content table-responsive">
              @include('admin.layouts.msg')
                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                        <tr>
                            <th>Sl no</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $key =>$item)
                        <tr>
                            <td>{{$key +1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->category->name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->price}}</td>

                            <td><img src="{{url('uploads/items/',$item->image)}}" style="border-radius:5px; height:80px; width:100px;" alt=""></td>
                            <td>
                                <ul class="list-inline">

                                    <li class="list-inline-item"><a class="btn btn-sm btn-info" href="{{url( 'admin/items/'.$item->id.'/edit')}}" ><i class="fa fa-pencil"></i></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open(['url'=>['admin/items/'.$item->id],'method'=>'delete']) !!}
                                        {!! Form::button("<i class='fa fa-trash'></i>",[
                                        'type'=>'submit',
                                        'onClick'=>"return confirm('Are You sure Delete $item->name ?')",
                                        'class'=>'btn btn-sm btn-danger'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                     @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush