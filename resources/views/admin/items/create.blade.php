@extends('admin.layouts.master')
@section('title','items')
@section('content')
    <div class="col-md-12">

    <div class="card">

        <div class="card-header" data-background-color="purple">
            <h4 class="title">Items
                <span class="pull-right"><a href="{{url('admin/items')}}" ><i class="material-icons">list</i>All Items</a></span>
            </h4>
        </div>
      @include('admin.layouts.msg')
        <div class="card-content">

            <div class="row">
                {!! Form::open(['url' => 'admin/items/', 'files'=>true]) !!}
                @include('admin.items.form')

                <div class="form-group">
                    {!! Form::submit('Add',["class"=>"btn btn-primary"]) !!}
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    </div>
@endsection