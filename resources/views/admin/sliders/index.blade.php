@extends('admin.layouts.master')
@section('title','Slider')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">All Slider
                    <span class="pull-right"><a href="{{url('admin/sliders/create')}}" title="Add Slider"><i class="material-icons">note_add</i>Add Slider</a></span>
                </h4>

            </div>
            <div class="card-content table-responsive">
              @include('admin.layouts.msg')
                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                        <tr>
                            <th>Sl no</th>
                            <th>Title</th>
                            <th>Sub Title</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($sliders as $slider)
                        <tr>
                            <td>{{$slider->id}}</td>
                            <td>{{$slider->title}}</td>
                            <td>{{$slider->sub_title}}</td>
                            <td><img src="{{url('uploads/sliders/',$slider->image)}}" style="border-radius:5px; height:80px; width:160px;" alt=""></td>
                            <td>
                                <ul class="list-inline">

                                    <li class="list-inline-item"><a class="btn btn-sm btn-info" href="{{url( 'admin/sliders/'.$slider->id.'/edit')}}" ><i class="fa fa-pencil"></i></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open(['url'=>['admin/sliders/'.$slider->id],'method'=>'delete']) !!}
                                        {!! Form::button("<i class='fa fa-trash'></i>",[
                                        'type'=>'submit',
                                        'onClick'=>"return confirm('Are You sure Delete $slider->slider ?')",
                                        'class'=>'btn btn-sm btn-danger'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                     @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush