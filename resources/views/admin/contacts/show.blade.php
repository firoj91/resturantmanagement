@extends('admin.layouts.master')
@section('title','edit')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Contacts
                    <span class="pull-right btn btn-sm btn-info"><a href="{{url('admin/contacts')}}">Back</a></span>
                </h4>

            </div>
            <div class="card-content">
                @foreach($contacts as $contact)
                    <p><strong>Name- </strong>{{$contact->name}}</p>
                    <p><strong>Email- </strong>{{$contact->email}}</p>
                    <p><strong>Subject - </strong>{{$contact->subject}}</p>
                    <p><strong>Message- </strong>{{$contact->message}}</p>
                 @endforeach
            </div>
        </div>
    </div>
@endsection

