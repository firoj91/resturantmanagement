@extends('admin.layouts.master')
@section('title','Contact')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Contacts</h4>

            </div>
            <div class="card-content table-responsive">
              @include('admin.layouts.msg')
                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                        <tr>
                            <th>Sl no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $key => $contact)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$contact->name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->subject}}</td>

                            <td>
                                <a href="{{route('contact.show',$contact->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> </a>

                                <form action="{{route('contact.destroy',$contact->id)}}" id="delete-form-{{$contact->id}}" style="display: none;" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button type="button" class="btn btn-sm btn-danger" onclick="if(confirm('Are you sure to Delete?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{$contact->id}}').submit();
                                        }else {
                                        event.preventDefault();
                                        }">
                                    <i class="material-icons">delete</i>

                                </button>
                            </td>
                        </tr>
                     @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush