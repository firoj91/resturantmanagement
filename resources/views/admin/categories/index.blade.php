@extends('admin.layouts.master')
@section('title','category')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Categories
                    <span class="pull-right"><a href="{{url('admin/categories/create')}}" title="Add Caterory"><i class="material-icons">note_add</i>Add New</a></span>
                </h4>

            </div>
            <div class="card-content table-responsive">
              @include('admin.layouts.msg')
                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                        <tr>
                            <th>Sl no</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->slug}}</td>
                            <td>{{$category->created_at}}</td>
                            <td>{{$category->updated_at}}</td>

                            <td>
                                <ul class="list-inline">

                                    <li class="list-inline-item"><a class="btn btn-sm btn-info" href="{{url( 'admin/categories/'.$category->id.'/edit')}}" ><i class="fa fa-pencil"></i></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open(['url'=>['admin/categories/'.$category->id],'method'=>'delete']) !!}
                                        {!! Form::button("<i class='fa fa-trash'></i>",[
                                        'type'=>'submit',
                                        'onClick'=>"return confirm('Are You sure Delete $category->slider ?')",
                                        'class'=>'btn btn-sm btn-danger'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                     @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush