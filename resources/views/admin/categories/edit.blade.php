@extends('admin.layouts.master')
@section('title','slider')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Category
                    <span class="pull-right"><a href="{{url('admin/categories')}}" ><i class="material-icons">list</i>All Category</a></span>
                </h4>

            </div>
           @include('admin.layouts.msg')
            <div class="card-content">
                <div class="row">
                    {!! Form::model( $category, ['url' => ['admin/categories', $category->id], 'method' => 'put']) !!}

                    @include('admin.categories.form')

                    <div class="form-group">

                        {!! Form::submit('Update', ["type"=>"submit", "class" => "btn btn-primary"]) !!}

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection