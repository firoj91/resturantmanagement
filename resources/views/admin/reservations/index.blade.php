@extends('admin.layouts.master')
@section('title','Reservation')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Reservations</h4>
            </div>
            <div class="card-content table-responsive">
              @include('admin.layouts.msg')
                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                        <tr>
                            <th>Sl no</th>
                            <th>Name</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Date and Time</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($reservation as $key => $reserve)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$reserve->name}}</td>
                            <td>{{$reserve->phone}}</td>
                            <td>{{$reserve->email}}</td>
                            <td>{{$reserve->date}}</td>
                            <td>{{$reserve->message}}</td>
                            <td>
                                @if($reserve->status == true)
                                    <span class="label label-info">Confirmed</span>
                                   @else
                                    <span class="label label-danger">not confirm yet</span>
                                @endif

                            </td>

                            <td>
                                @if($reserve->status == false)
                                    <form id="status-form-{{$reserve->id}}" action="{{route('reservation.status',$reserve->id)}}" style="display: none;" method="post">
                                            @csrf
                                    </form>
                                    <button type="button" class="btn btn-sm btn-info" onclick="if(confirm('Are yoy verify this request by phone?')){
                                        event.preventDefault();
                                        document.getElementById('status-form-{{$reserve->id}}').submit();
                                    }else{
                                        event.preventDefault();
                                    }"><i class="material-icons">done</i>

                                    </button>
                                @endif

                                    <form action="{{route('reservation.destroy',$reserve->id)}}" id="delete-form-{{$reserve->id}}" style="display: none;" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <button type="button" class="btn btn-sm btn-danger" onclick="if(confirm('Are you sure to Delete?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{$reserve->id}}').submit();
                                    }else {
                                        event.preventDefault();
                                            }">
                                        <i class="material-icons">delete</i>

                                    </button>

                                  {{--{!! Form::open(['route'=>['reservation.destroy',$reserve->id],'method'=>'delete']) !!}--}}
                                        {{--{!! Form::button("<i class='fa fa-trash'></i>",[--}}
                                        {{--'type'=>'submit',--}}
                                        {{--'onClick'=>"return confirm('Are You sure Delete this ?')",--}}
                                        {{--'class'=>'btn btn-sm btn-danger'--}}
                                        {{--]) !!}--}}
                                    {{--{!! Form::close() !!}--}}


                            </td>
                        </tr>
                     @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush