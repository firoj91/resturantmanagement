<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>admin/ @yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    {!! Html::style('backend/css/bootstrap.min.css') !!}
    {!! Html::style('backend/css/material-dashboard.css?v=1.2.0') !!}
    {!! Html::style('backend/css/demo.css') !!}

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    @stack('css')

</head>

<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">

        <div class="logo">
            <a href="http://www.csfworld.com" class="simple-text">
               Creative system
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="{{Request::is('admin/dashboard*')? 'active':''}}">
                    <a href="{{url('admin/dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="{{Request::is('admin/sliders*')? 'active':''}}">
                    <a href="{{url('admin/sliders')}}">
                        <i class="material-icons">slideshow</i>
                        <p>Sliders</p>
                    </a>
                </li>
                <li class="{{Request::is('admin/categories*')? 'active':''}}">
                    <a href="{{url('admin/categories')}}">
                        <i class="material-icons">content_paste</i>
                        <p>Categories</p>
                    </a>
                </li>
                <li class="{{Request::is('admin/items*')? 'active' : ''}}">
                    <a href="{{url('admin/items')}}">
                        <i class="material-icons">library_books</i>
                        <p>Items</p>
                    </a>
                </li>

                <li class="{{Request::is('admin/reservations*')? 'active' : ''}}">
                    <a href="{{url('admin/reservations')}}">
                        <i class="material-icons">view_list</i>
                        <p>Reservations</p>
                    </a>
                </li>

                <li class="{{Request::is('admin/contacts*')? 'active' : ''}}">
                    <a href="{{route('contact.index')}}">
                        <i class="material-icons">message</i>
                        <p>Contacts</p>
                    </a>
                </li>


            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('admin/dashboard')}}"> Resturant Dashboard </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">person_pin</i>
                                {{Auth::user()->name}}
                                <p class="hidden-lg hidden-md">Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="material-icons">exit_to_app</i>
                                <p class="hidden-lg hidden-md">logout</p>
                            </a>
                            <form action="{{route('logout')}}" method="post" id="logout-form" style="display:none;">
                                @csrf
                            </form>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">

                <p class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    <a href="http://www.csfworld.com">www.csfworld.com</a>, Md Fe<sup>R</sup>dous Hasan
                </p>
            </div>
        </footer>
    </div>
</div>

</body>
<!--   Core JS Files   -->
{!! Html::script('backend/js/jquery-3.2.1.min.js') !!}
{!! Html::script('backend/js/bootstrap.min.js') !!}
{!! Html::script('backend/js/material.min.js') !!}
{!! Html::script('backend/js/chartist.min.js') !!}
{!! Html::script('backend/js/arrive.min.js') !!}
{!! Html::script('backend/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('backend/js/bootstrap-notify.js') !!}
{!! Html::script('backend/js/material-dashboard.js?v=1.2.0') !!}
{!! Html::script('backend/js/demo.js') !!}
{!! Html::script('backend/') !!}

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>
@stack('scripts')
{!! Toastr::message() !!}
</html>