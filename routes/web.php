<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//backend----


Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');
Route::post('/reservation','ReservationController@reserve')->name('reservation.reserve');
Route::post('/contact','ContactController@sendMessage')->name('contact.send');


Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'admin'], function(){
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::resource('sliders','SliderController');
    Route::resource('categories','CategoryController');
    Route::resource('items','ItemController');

    Route::get('reservations','ReservationController@index')->name('reservation.index');
    Route::post('reservations/{id}','ReservationController@status')->name('reservation.status');
    Route::delete('reservations/{id}','ReservationController@destroy')->name('reservation.destroy');

    Route::get('contacts','ContactController@index')->name('contact.index');
    Route::get('contacts/{id}','ContactController@show')->name('contact.show');
    Route::delete('contacts/{id}','ContactController@destroy')->name('contact.destroy');
});
